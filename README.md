# DevOps_CompteRendu

## Mardi 20 Octobre :
  * création de scenarios

  * choix de la techno: java spring

## Mardi 3 Novembre :
  * création du fichier de compte rendu

  * décision d'utiliser gradle

  * mise en place d'un squelette de code

  * décision d'utiliser MySQL pour la base de donné

  * objectif pour le mardi 10 Novembre:
    * avoir des tests unitaires
    * avoir des tests d’intégration continu
    * déployer
